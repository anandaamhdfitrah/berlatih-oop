<?php

require_once('Animal.php');
require_once('Frog.php');
require_once('Ape.php');

$shaun = new Animal("shaun");
echo "Name = " . $shaun->name . "<br>";
echo "legs = " . $shaun->legs . "<br>";
echo "cold_blooded = " . $shaun->cold_blooded . "<br><br>";

$frog = new Kodok("buduk");
echo "Name = ". $frog->name . "<br>";
echo "leg = ". $frog->legs . "<br>";
echo "cold_blooded = " .$frog->cold_blooded . "<br>";
echo "Jump = " . $frog->jump . "<br><br>";

$sungokong = new Ape("kera sakti");
echo "Name = ". $sungokong->name . "<br>";
echo "leg = ". $sungokong->legs . "<br>";
echo "cold_blooded = " .$sungokong->cold_blooded . "<br>";
echo "Yell = " .$sungokong->yell . "<br><br>";

?>